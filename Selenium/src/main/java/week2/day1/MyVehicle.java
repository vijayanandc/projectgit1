package week2.day1;

public abstract class MyVehicle implements Vehicle{
	
	public void vehicle_model()
	{
		System.out.println("My vehicle model");
	}
	
	public abstract void light();
	
	public void brake()
	{
		System.out.println("Braking system");
	}

}
