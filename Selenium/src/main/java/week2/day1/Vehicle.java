package week2.day1;

public interface Vehicle {
	
	public void headlamp();
	
	public void brake();
	
	public void seatsize();

}
