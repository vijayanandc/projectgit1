package week3.day1;

import java.awt.RenderingHints.Key;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IrctcUsersignup {

	public static void main(String[] args) throws Throwable {
		
		
System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		
		driver.findElement(By.id("userRegistrationForm:userName")).sendKeys("user");
		
		driver.findElement(By.id("userRegistrationForm:password")).sendKeys("pass");
		
		driver.findElement(By.id("userRegistrationForm:confpasword")).sendKeys("pass");
		
		Select s = new Select (driver.findElement(By.id("userRegistrationForm:securityQ")));
		
		s.selectByValue("2");
		
		driver.findElement(By.id("userRegistrationForm:securityAnswer")).sendKeys("sec ans");
		
		Select s1 = new Select (driver.findElement(By.id("userRegistrationForm:prelan")));
		
		s1.selectByIndex(0);

		driver.findElement(By.id("userRegistrationForm:firstName")).sendKeys("FN");
		driver.findElement(By.id("userRegistrationForm:middleName")).sendKeys("Mname");
		driver.findElement(By.id("userRegistrationForm:lastName")).sendKeys("LN");
		
		driver.findElement(By.id("userRegistrationForm:gender:0")).click();
		driver.findElement(By.id("userRegistrationForm:maritalStatus:0")).click();
		
		//
		
		Select s2 = new Select(driver.findElement(By.id("userRegistrationForm:occupation")));
		
		s2.selectByVisibleText("Government");
		
		driver.findElement(By.id("userRegistrationForm:uidno")).sendKeys("1234");
		
		driver.findElement(By.id("userRegistrationForm:idno")).sendKeys("AKW");
		
		Select s3 = new Select(driver.findElement(By.id("userRegistrationForm:countries")));
		
		s3.selectByIndex(1);
		
		driver.findElement(By.id("userRegistrationForm:email")).sendKeys("email@gmail.com");
		
		//driver.findElement(By.id("userRegistrationForm:isdCode")).sendKeys("91");
		//driver.findElement(By.id("userRegistrationForm:mobile")).sendKeys("994022");
		
		Select s4 = new Select(driver.findElement(By.id("userRegistrationForm:nationalityId")));
		
		s4.selectByIndex(1);
		
		driver.findElement(By.id("userRegistrationForm:address")).sendKeys("no 11");
		
		driver.findElement(By.id("userRegistrationForm:street")).sendKeys("street");
		
		driver.findElement(By.id("userRegistrationForm:area")).sendKeys("area");
		
		driver.findElement(By.id("userRegistrationForm:pincode")).sendKeys("600001");
		
		driver.findElement(By.id("userRegistrationForm:statesName")).click();
		
		Thread.sleep(5000);
		
		Select s5 = new Select (driver.findElement(By.id("userRegistrationForm:cityName")));
		s5.selectByValue("Chennai");
		
		Thread.sleep(5000);
		
		Select s6 = new Select (driver.findElement(By.id("userRegistrationForm:postofficeName")));
		s6.selectByVisibleText("Chennai G.P.O. ");
		driver.findElement(By.id("userRegistrationForm:landline")).sendKeys("999");
		
		driver.findElement(By.id("userRegistrationForm:resAndOff:0")).click();
		
	}

}
