package week3.day1;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLead {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.get("http://leaftaps.com/opentaps");
		
		driver.findElement(By.id("username")).sendKeys("DemoSalesManager");
		
		driver.findElement(By.id("password")).sendKeys("crmsfa");
		
		driver.findElement(By.className("decorativeSubmit")).click();
		
		driver.findElement(By.linkText("CRM/SFA")).click();
		
		driver.findElement(By.linkText("Create Lead")).click();
		
		driver.findElement(By.id("createLeadForm_companyName")).sendKeys("BNY");
		
		driver.findElement(By.id("createLeadForm_firstName")).sendKeys("VJF");
		
		driver.findElement(By.id("createLeadForm_lastName")).sendKeys("VJL");
		
		WebElement source = driver.findElement(By.id("createLeadForm_dataSourceId"));
		
		Select s = new Select(source);
		
		s.selectByIndex(1);
		
WebElement source1 = driver.findElement(By.id("createLeadForm_industryEnumId"));
		
		Select s1 = new Select(source1);
		
		s1.selectByVisibleText("Distribution");
		
		
WebElement source2 = driver.findElement(By.id("createLeadForm_ownershipEnumId"));
		
		Select s2 = new Select(source2);
		
		List<WebElement> alloptions = s2.getOptions();
		
		for (WebElement eachopt : alloptions) {
			
			eachopt.getText();
			
		}
		
		int size = alloptions.size();
		
		s2.selectByIndex(size - 1);
		
		
		
		
		
		
		driver.findElement(By.name("submitButton")).click();
		
		//driver.close();
		

	}

}
