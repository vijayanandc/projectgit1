package week3.day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AUI {

	public static void main(String[] args) {
		
		
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		driver.get("https://www.flipkart.com/");
		
		driver.getKeyboard().sendKeys(Keys.ESCAPE);
		
		WebElement ele = driver.findElementByXPath("//span[text()='Electronics']");
		
		Actions builder = new Actions(driver);
		
		builder.moveToElement(ele).perform();
		
		driver.findElementByLinkText("Samsung").click();
		
		/*WebDriverWait wait = new WebDriverWait(driver, 20);
		
		WebElement sam = driver.findElementByLinkText("Samsung");
		
		wait.until(ExpectedConditions.elementToBeClickable(sam));*/

	}

}
