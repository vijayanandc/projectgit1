package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.omg.Messaging.SYNC_WITH_TRANSPORT;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {

	public static void main(String[] args) throws Throwable {
		
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.get("http://leaftaps.com/opentaps/control/main");
		
		driver.findElement(By.id("username")).sendKeys("DemoSalesManager");
		
		driver.findElement(By.id("password")).sendKeys("crmsfa");
		
		driver.findElement(By.className("decorativeSubmit")).click();
		
		driver.findElement(By.linkText("CRM/SFA")).click();
		
		driver.findElement(By.linkText("Leads")).click();
		
		driver.findElementByLinkText("Merge Leads").click();
		
		String parent = driver.getWindowHandle();
		
		driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
		
		
		Set<String> allwin = driver.getWindowHandles();
		
		//.get method is not in set, so we need to copy Set into List
		
		List<String> ls = new ArrayList<>();
		
		ls.addAll(allwin);
		
		driver.switchTo().window(ls.get(1));
		
		driver.findElementByName("firstName").sendKeys("vj1");
		
		driver.findElementByXPath("//button[@class='x-btn-text']").click();
		
		Thread.sleep(2000);
		
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		
		driver.switchTo().window(parent);
		
		driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
		
		Set<String> allwin1 = driver.getWindowHandles();
		
		//.get method is not in set, so we need to copy Set into List
		
		List<String> ls1 = new ArrayList<>();
		
		ls1.addAll(allwin1);
		
		driver.switchTo().window(ls1.get(1));
		
		//driver.switchTo().window(ls.get(1));
		
		driver.findElementByName("id").sendKeys("10260");
		
		driver.findElementByXPath("(//button[@class='x-btn-text'])[1]").click();
		
		Thread.sleep(2000);
		
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		
		driver.switchTo().window(parent);
		
		driver.findElementByXPath("//a[@class='buttonDangerous']").click();
		
		driver.switchTo().alert().accept();
		
		driver.close();


		
		
	
		
		

		
	}

}
