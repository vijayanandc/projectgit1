package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class Frames {

	public static void main(String[] args) {
		
		
System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		
		driver.switchTo().frame("iframeResult");
		
		driver.findElementByXPath("//button[text()='Try it']").click();
		
		System.out.println(driver.switchTo().alert().getText());
		Alert alert = driver.switchTo().alert();
		


		driver.switchTo().alert().sendKeys("Vijayanand");
		driver.switchTo().alert().accept();
		
		
		
		String t = driver.findElementByXPath("//*[@id=\"demo\"]").getText();
		
		System.out.println(t);
		
		String name = "Vijayanand";
		
		if(t.contains(name))
		{
			System.out.println("success!!!!");
		}
		else {
			System.out.println("fail");
		}
		
		

	}

}
