package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class Windows {

	public static void main(String[] args) {
		
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.get("https://www.irctc.co.in/eticketing/loginHome.jsf");
		
		driver.findElementByLinkText("AGENT LOGIN").click();
		
		String parent = driver.getWindowHandle();
		
		driver.findElementByLinkText("Contact Us").click();
		
		Set<String> allwin = driver.getWindowHandles();
		
		//.get method is not in set, so we need to copy Set into List
		
		List<String> ls = new ArrayList<>();
		
		ls.addAll(allwin);
		
		driver.switchTo().window(ls.get(1));
		
		String title = driver.getTitle();
		
		System.out.println(title);
		
		driver.switchTo().window(parent);
		
		driver.close();
		
		
		
		
		
		

	}

}
