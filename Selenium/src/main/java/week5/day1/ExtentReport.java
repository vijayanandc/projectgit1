package week5.day1;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReport {
	
	static ExtentHtmlReporter reporter;
	static ExtentReports extent;
	@BeforeSuite
	public void startreport()
	{
		reporter = new ExtentHtmlReporter("./reports/result.html");
		reporter.setAppendExisting(true);
		extent = new ExtentReports();
		extent.attachReporter(reporter);
	}
	@Test
	public void report() throws IOException
	{
		ExtentTest test = extent.createTest("createlead", "new lead");
		test.assignAuthor("vijay");
		test.assignCategory("smoke");
		test.pass("username entered");
		test.fail("failed to enter", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/snaps1.png").build());

	}
	
	@AfterSuite
	public void flush() {
		
		extent.flush();
			
		}
	}


